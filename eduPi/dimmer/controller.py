class Controller:
    lightOn = False
    
    def switchLight(self, led, ui):
        if (self.lightOn == False):
            led.turnOn()
            ui.setDisplayText("An")
            self.lightOn = True
        else:
            led.turnOff()
            ui.setDisplayText("Aus")
            self.lightOn = False
            