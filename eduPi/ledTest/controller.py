class Controller:
    
    def start(self, led, ui):
        led.turnOn()
        ui.setDisplayText("Hello World!")
        
    def stop(self, led, ui):
        led.turnOff()
        ui.clearDisplay()