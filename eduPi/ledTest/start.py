#!/usr/bin/env python
from eduPi.basics.ui import UI
from eduPi.basics.led import LED
from controller import Controller
import RPi.GPIO as GPIO
import sys
import os

led = None
controller = None
ui = None

def main():    
    # Bootstrap
    global led
    led = LED(LED.GPIO_4)
    global ui
    ui = UI()
    global controller
    controller = Controller()
    controller.start(led, ui)
    while(True):
        # no op
        1 + 1
    

if __name__ == "__main__":
  try:
    print('\nPress CTRL + C to exit')
    main()
  except KeyboardInterrupt:
    print('\nExit requested')
    controller.stop(led, ui)
    try:
      sys.exit(0)
    except SystemExit:
      os._exit(0)