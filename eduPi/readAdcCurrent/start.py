#!/usr/bin/env python
from eduPi.basics.ui import UI
from eduPi.basics.led import LED
from eduPi.basics.voltageSensor import VoltageSensor
from eduPi.basics.currentSensor import CurrentSensor
from controller import Controller
import RPi.GPIO as GPIO
import sys
import os
import time

controller = None
ui = None
voltageSensor = None

def main():    
    global voltageSensor
    voltageSensor = VoltageSensor()
    currentSensor = CurrentSensor(voltageSensor)
    
    global ui
    ui = UI()
    
    global controller
    controller = Controller()
    controller.start(ui)
    
    controller.calibrate(currentSensor)
    
    while(True):
        if (ui.isSelectButtonPressed()):
          controller.calibrate(currentSensor)
        controller.readVoltage(ui, voltageSensor)
        controller.readCurrent(ui, currentSensor)
        ui.displayValues()
        time.sleep(.5)
    

if __name__ == "__main__":
  try:
    print('\nPress CTRL + C to exit')
    main()
  except KeyboardInterrupt:
    print('\nExit requested')
    controller.stop(ui)
    try:
      sys.exit(0)
    except SystemExit:
      os._exit(0)