from eduPi.basics.ui import UI

class Controller:
    
    def start(self, ui):
        ui.clearDisplay()
        
    def stop(self, ui):
        ui.clearDisplay()
        
    def readVoltage(self, ui, voltageSensor):
        valueRaw = voltageSensor.measureRaw()
        value = valueRaw * voltageSensor.getConversionFactor()
        # ui.setDisplayText("%.3f V" % (value))
        ui.displayValue("%.3f V" % (value), UI.TOP_LEFT)
        
    def readCurrent(self, ui, currentSensor):
        valueRaw = currentSensor.measureRaw()
        value = valueRaw
        value = valueRaw * currentSensor.getConversionFactor()
        # ui.setDisplayText("%.3f V" % (value))
        ui.displayValue("%.0f mA" % (value), UI.TOP_RIGHT)
        
    def calibrate(self, currentSensor):
        currentSensor.calibrate()