class Controller:
    def selectButtonChanged(self, pressed, led, ui):
        if (pressed):
            led.turnOn()
            ui.setDisplayText("An")
        else:
            led.turnOff()
            ui.setDisplayText("Aus")
            