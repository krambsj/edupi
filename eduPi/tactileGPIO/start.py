#!/usr/bin/env python
from eduPi.basics.ui import UI
from eduPi.basics.led import LED
from controller import Controller
import RPi.GPIO as GPIO
import time
import sys
import os

led = None

def main():    
    global led
    # Bootstrap
    led = LED(LED.GPIO_4)
    ui = UI()
    controller = Controller()
    
    # Test sequence
    ui.setDisplayText("Teste LED...")
    led.turnOn()
    time.sleep(3)
    led.turnOff()
    
    # Prepare event loop
    pressed = False
    ui.clearDisplay()
    ui.setDisplayText("Aus")
    
    # Event loop
    while(True):
        if (not pressed and ui.isSelectButtonPressed()):
            controller.selectButtonChanged(True, led, ui)
            pressed = True
        elif (pressed and not ui.isSelectButtonPressed()):
            controller.selectButtonChanged(False, led, ui)
            pressed = False
            

if __name__ == "__main__":
  try:
    print('\nPress CTRL + C to exit')
    main()
  except KeyboardInterrupt:
    print('\nExit requested')
    led.turnOff()
    try:
      sys.exit(0)
    except SystemExit:
      os._exit(0)