from eduPi.basics.ui import UI

class Controller:
    
    def start(self, ui):
        ui.clearDisplay()
        
    def stop(self, ui):
        ui.clearDisplay()
        
    def readVoltage(self, ui, voltageSensor):
        valueRaw = voltageSensor.measureRaw()
        value = valueRaw * voltageSensor.getConversionFactor()
        ui.displayValue("%.3f V" % (value), UI.TOP_LEFT)
        return value
        
    def readCurrent(self, ui, currentSensor):
        valueRaw = currentSensor.measureRaw()
        value = valueRaw
        value = valueRaw * currentSensor.getConversionFactor()
        ui.displayValue("%.0f mA" % (value), UI.TOP_RIGHT)
        return value
        
    def calibrate(self, currentSensor):
        currentSensor.calibrate()
        
    def calculatePower(self, ui, voltage, current):
        power = voltage * current
        ui.displayValue("%.0f mW" % (power), UI.BOTTOM_LEFT)
        
    def calculateResistance(self, ui, voltage, current):
        if (current == 0):
            resistance = 0
        else:
            resistance = voltage / current * 1000
            
        ui.displayValue("%.0f \x01" % (resistance), UI.BOTTOM_RIGHT)
        