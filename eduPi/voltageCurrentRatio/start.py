#!/usr/bin/env python
from eduPi.basics.ui import UI
from eduPi.basics.led import LED
from eduPi.basics.voltageSensor import VoltageSensor
from eduPi.basics.currentSensor import CurrentSensor
from eduPi.basics.dac import DAC
from controller import Controller
import RPi.GPIO as GPIO
import sys
import os
import time

controller = None
ui = None
voltageSensor = None
dac = None

def main():    
    global voltageSensor
    voltageSensor = VoltageSensor()
    currentSensor = CurrentSensor(voltageSensor)
    global dac
    dac = DAC()
    
    global ui
    ui = UI()
    
    global controller
    controller = Controller()
    controller.start(ui, dac)
    
    controller.calibrate(currentSensor)
    
    pressed = False
    
    while(True):
        if (ui.isSelectButtonPressed()):
          controller.calibrate(currentSensor)
          continue
          
        if (not pressed and (ui.isUpButtonPressed() or ui.isDownButtonPressed())):
            pressed = True
        elif (pressed):
          if (ui.isUpButtonPressed()):
            controller.increase_voltage(dac, ui)
            pressed = False
          elif (ui.isDownButtonPressed()):
            controller.decrease_voltage(dac, ui)
            pressed = False
          
        controller.read_voltage(ui, voltageSensor)
        controller.read_current(ui, currentSensor)
        ui.displayValues()
        time.sleep(.5)
    

if __name__ == "__main__":
  try:
    print('\nPress CTRL + C to exit')
    main()
  except KeyboardInterrupt:
    print('\nExit requested')
    controller.stop(ui, dac)
    try:
      sys.exit(0)
    except SystemExit:
      os._exit(0)