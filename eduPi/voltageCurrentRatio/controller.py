from eduPi.basics.ui import UI

class Controller:
    
    def start(self, ui, dac):
        dac.set_voltage(0)
        ui.displayValue("%.0f V" % dac.voltage, UI.BOTTOM_RIGHT)
        ui.clearDisplay()
        
    def stop(self, ui, dac):
        dac.set_voltage(0)
        ui.clearDisplay()
        
    def read_voltage(self, ui, voltageSensor):
        valueRaw = voltageSensor.measureRaw()
        value = valueRaw * voltageSensor.getConversionFactor()
        # ui.setDisplayText("%.3f V" % (value))
        ui.displayValue("%.3f V" % (value), UI.TOP_LEFT)
        
    def read_current(self, ui, currentSensor):
        valueRaw = currentSensor.measureRaw()
        value = valueRaw
        value = valueRaw * currentSensor.getConversionFactor()
        # ui.setDisplayText("%.3f V" % (value))
        ui.displayValue("%.0f mA" % (value), UI.TOP_RIGHT)
        
    def calibrate(self, currentSensor):
        currentSensor.calibrate()
        
    def increase_voltage(self, dac, ui):
        dac.increase_voltage()
        ui.displayValue("%.0f V" % dac.voltage, UI.BOTTOM_RIGHT)
        
    def decrease_voltage(self, dac, ui):
        dac.decrease_voltage()
        ui.displayValue("%.0f V" % dac.voltage, UI.BOTTOM_RIGHT)
        