import RPi.GPIO as GPIO

class LED:
    pin = -1

    GPIO_2 = 3
    GPIO_3 = 5
    GPIO_4 = 7
    GPIO_5 = 29
    GPIO_6 = 31
    GPIO_7 = 26
    GPIO_8 = 24
    GPIO_9 = 21
    GPIO_10 = 19
    GPIO_11 = 23
    GPIO_12 = 32
    GPIO_13 = 33
    GPIO_14 = 8
    GPIO_15 = 10
    GPIO_16 = 36
    GPIO_17 = 11
    GPIO_18 = 12
    GPIO_19 = 35
    GPIO_20 = 38
    GPIO_21 = 40
    GPIO_22 = 15
    GPIO_23 = 16
    GPIO_24 = 18
    GPIO_25 = 22
    GPIO_27 = 13

    def __init__(self, pin):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD) ## Use board pin numbering
        self.pin = pin
        GPIO.setup(self.pin, GPIO.OUT) ## Setup GPIO Pin 7 to OUT
        
    def turnOn(self):
        GPIO.output(self.pin, True)
        
    def turnOff(self):
        GPIO.output(self.pin, False)