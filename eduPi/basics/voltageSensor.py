from lib.adafruit.Adafruit_ADS1x15 import ADS1115
from lib.adafruit.Adafruit_ADS1x15 import ADS1x15_CONFIG_GAIN
from eduPi.basics.sampler import Sampler

class VoltageSensor:
    adc = None
    mode = 3
    gain = 2/3
    data_rate = 8
    sampler = None

    def __init__(self, mode = 3):
        self.mode = mode
        self.adc = ADS1115()
        self.sampler = Sampler(2)
       
    def measure(self):
        value = self.measureRaw()
        value = value * self.getConversionFactor()
        return '%0.2f' % value # #05
    
    def measureRaw(self):
        value = self.adc.read_adc_difference(self.mode, gain=self.gain, data_rate=self.data_rate)
        return self.sampler.sample(value)
        
    def getConversionFactor(self):
        return 1.0 / 32768.0 * (4096.0 / (2.0 / 3.0)) / 1000.0
        
    def _measureSingle(self, analogIn=0,gain=2/3,data_rate=8):
        return self.adc.read_adc(analogIn, gain=gain, data_rate=data_rate)