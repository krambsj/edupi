from lib.adafruit import Adafruit_CharLCDPlate as AdafruitLCD

class LCD:
    lcd = None

    def __init__(self):
        self.lcd = LCD.Adafruit_CharLCDPlate()
        self.lcd.clear()
        
    def clear(self):
        self.lcd.clear()
        
    def message(self, message):
       self.lcd.message(message)