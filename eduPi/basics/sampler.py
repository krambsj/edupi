class Sampler:
  samples = []
  max = 0
  lastValue = 0
  
  def __init__(self, maxSamples):
    self.max = maxSamples
    
  def sample(self, value):
    self.samples.append(value)
    if (len(self.samples) >= self.max):
      return self.average()
    else:
      return self.lastValue
      
  def average(self, clear = True):
    sum = 0
    for sample in self.samples:
      sum += sample
      
    avg = sum / len(self.samples)
    if (clear):
      self.samples = []
      
    self.lastValue = avg
    return avg