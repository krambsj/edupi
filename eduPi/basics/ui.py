from lib.adafruit import Adafruit_CharLCDPlate as AdafruitLCD

class UI:
    lcd = None
    values = [ '', '', '', '']
    displayWidth = 16
    
    TOP_LEFT     = 0
    TOP_RIGHT    = 1
    BOTTOM_LEFT  = 2
    BOTTOM_RIGHT = 3

    def __init__(self):
        self.lcd = AdafruitLCD.Adafruit_CharLCDPlate()
        
        # omega
        self.lcd.createChar(1, [0, 14, 17, 17, 17, 10, 27, 0])
        # infinity
        self.lcd.createChar(2, [0, 0,  0,  10, 21, 10, 0,  0])
        # bulb on
        # bulb off
        self.lcd.clear()
        
    def clearDisplay(self):
        self.lcd.clear()
        
    def setDisplayText(self, message):
        self.lcd.clear()
        self.lcd.message(message)
       
    def isSelectButtonPressed(self):
        return self.lcd.buttonPressed(AdafruitLCD.Adafruit_CharLCDPlate.SELECT)
       
    def isLeftButtonPressed(self):
        return self.lcd.buttonPressed(AdafruitLCD.Adafruit_CharLCDPlate.LEFT)
       
    def isRightButtonPressed(self):
        return self.lcd.buttonPressed(AdafruitLCD.Adafruit_CharLCDPlate.RIGHT)
       
    def isUpButtonPressed(self):
        return self.lcd.buttonPressed(AdafruitLCD.Adafruit_CharLCDPlate.UP)
       
    def isDownButtonPressed(self):
        return self.lcd.buttonPressed(AdafruitLCD.Adafruit_CharLCDPlate.DOWN)
        
    def displayValue(self, value, position, update = False):
        self.values[position] = value

        if (update):
            self.displayValues()
            
    def displayValues(self):
        message = '{0: <8}{1: >8}\n{2: <8}{3: >8}'.format(self.values[UI.TOP_LEFT], self.values[UI.TOP_RIGHT], self.values[UI.BOTTOM_LEFT], self.values[UI.BOTTOM_RIGHT])
        self.setDisplayText(message)
        
        