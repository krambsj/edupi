from lib.adafruit.Adafruit_MCP4725 import MCP4725

class DAC:
    dac = None
    voltage = 0
    _max = 4.0

    def __init__(self):
        self.dac = MCP4725()
        
    def set_voltage(self, voltage):
        self.voltage = voltage
        self.update_voltage()
        
    def update_voltage(self):
        self.voltage = max(0, min(self._max, self.voltage))
        self.dac.set_voltage(int(float(self.voltage) * 808.0)) #int(float(self.voltage) / self._max * 4096.0))
        
    def increase_voltage(self):
        self.set_voltage(self.voltage + 1)
        
    def decrease_voltage(self):
        self.set_voltage(self.voltage - 1)
        
    def set_voltage_1v(self):
        self.set_voltage(1)
        
    def set_voltage_2v(self):
        self.set_voltage(2)
        
    def set_voltage_3v(self):
        self.set_voltage(3)