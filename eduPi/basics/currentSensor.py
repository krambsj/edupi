from lib.adafruit.Adafruit_ADS1x15 import ADS1115
from lib.adafruit.Adafruit_ADS1x15 import ADS1x15_CONFIG_GAIN
from eduPi.basics.sampler import Sampler

class CurrentSensor:
    voltageSensor = None
    analogIn = 0
    offset = 0
    sampler = None

    def __init__(self, voltageSensor, analogIn = 0):
        self.analogIn = analogIn
        self.voltageSensor = voltageSensor
        self.sampler = Sampler(2)
       
    def measure(self):
        value = self.measureRaw()
        value = value * self.getConversionFactor()
        return '%0.1f' % value # #05
    
    def measureRaw(self):
        value = self.voltageSensor._measureSingle(self.analogIn)
        value = (value - self.offset)
        value = value * self.voltageSensor.getConversionFactor() * 1000
        return self.sampler.sample(value)
        
    def calibrate(self):
        self.offset = self.voltageSensor._measureSingle(self.analogIn)
        
    def getConversionFactor(self):
        return 1.0 / 9.65 # mA/mV
        