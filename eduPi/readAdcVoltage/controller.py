class Controller:
    
    def start(self, ui):
        ui.clearDisplay()
        
    def stop(self, ui):
        ui.clearDisplay()
        
    def read(self, ui, voltageSensor):
        valueRaw = voltageSensor.measureRaw()
        value = valueRaw * voltageSensor.getConversionFactor()
        ui.setDisplayText("%.3f V" % (value))