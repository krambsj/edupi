class Controller:
    lightOn = False
    
    def switchLight(self, led, ui):
        if (self.lightOn == False):
            led.turnOn()
            ui.setDisplayText("Lampe ist an")
            self.lightOn = True
        else:
            led.turnOff()
            ui.setDisplayText("Lampe ist aus")
            self.lightOn = False
            