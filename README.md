# EduPi - RPi & Python ready for classroom #

This library provides code lessons that can be used inside your classroom for teaching the Raspberry Pi together with python. 

Code lessons consist of `start.py` and `controller.py` files. The `start.py` file is where all the scary stuff is inside, you would not want to scare off your students with, while the `controller.py` is there for playing around or teaching a certain lesson. One way to do that is to remove one or more lines from the controller and give your students the task to realize a given behaviour. 

Currently implemented code lessons are:

* `ledTest`: Turns on an LED upon start and turns it off when the script is stopped (e.g. through pressing `CTRL + C`)
* `tactileGPIO`: Whenever a button is pressed, the `Controller` class gets informed about the new state of the button. By introducing an `if` block, an LED is turned on or off, respectively.
* `readAdcVoltage`: Reads the differential voltage between two connectors of an ADS1115 analog digital converter via i2c.

# Directory Structure #

/lib contains 3rd party libraries that are used for controlling or communicating with hardware components.
/eduPi/basics contains basic components that mostly encapsulate hardware components reduced to a very simple interface for hassle-free usage.
/eduPi/ contains the individual code lessons each structured as described above.

# Usage #

In order to execute on of these demo cases just call the respective start file (e.g. `ledTest`):

```
$ sudo python -m eduPi.ledTest.start
```